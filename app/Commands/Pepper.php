<?php

namespace App\Commands;

require __DIR__ . '/../../vendor/autoload.php';

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Pepper extends Command
{
  protected static $defaultName = 'pepper';

  protected function configure()
  {
    $this->setDescription('Grabs a Git Repo and layers the directory & file changes on top of your Thiccnr project.')
         ->setHelp('Create a new basic Thiccnr project')
         ->addArgument('repoPath', InputArgument::REQUIRED, 'Path to Git Repository');
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $output->writeln([
      "Adding the ingrediants..."
    ]);

    //$dirName = 'thiccnr-master';
    //$zipFileName = $dirName . '.zip';

    //$client = new \GuzzleHttp\Client(['base_uri' => 'https://git.uiowa.edu/jseeley/thiccnr/-/archive/']);
    //$response = $client->request('GET', 'master/' . $zipFileName);

    //$filesystem = new \Symfony\Component\Filesystem\Filesystem;
    //$filesystem->dumpFile($zipFileName, $response->getBody());

    //$zip = new \ZipArchive;
    //$res = $zip->open($zipFileName);
    //$tmpPath = './thiccnrprojecttmp';
    //if($res === TRUE) {
      //$zip->extractTo($tmpPath);
      //$zip->close();
    //} else {
      //echo 'An error has occurred';
    //}

    //$output->writeln([
      //"Warming up the soup..."
    //]);
    //$filesystem->remove($zipFileName);
    //$filesystem->mirror($tmpPath . '/' . $dirName, $input->getArgument('projectName'));
    //$filesystem->remove($tmpPath);

    //$output->writeln([
      //"Dinner is served..."
    //]);

  }
}
